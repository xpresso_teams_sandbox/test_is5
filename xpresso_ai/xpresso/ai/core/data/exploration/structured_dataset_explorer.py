__all__ = ['StructuredDatasetExplorer']
__author__ = 'Srijan Sharma'

import itertools
import re

import pandas as pd
import scipy.stats as stats
import tqdm

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidDatatypeException, InvalidDateFormat, ExceedsDataSize
from xpresso.ai.core.commons.utils.constants import DEFAULT_GARBAGE_THRESHOLD, \
    MAX_DATE_LENGTH, MIN_DATE_LENGTH, EXPLORE_UNIVARIATE_FILENAME, \
    EXPLORER_OUTPUT_PATH, EXPLORE_MULTIVARIATE_FILENAME, \
    DEFAULT_PROBABILITY_BINS, MAX_DATASET_SIZE_GB
from xpresso.ai.core.data.automl.attribute_info import AttributeInfo
from xpresso.ai.core.data.automl.data_type import DataType
from xpresso.ai.core.data.automl.dataset_type import DECIMAL_PRECISION
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.automl.utils import get_dataframe_size
from xpresso.ai.core.data.exploration.explore_categorical import ExploreCategory
from xpresso.ai.core.data.exploration.explore_date import ExploreDate
from xpresso.ai.core.data.exploration.explore_numeric import ExploreNumeric
from xpresso.ai.core.data.exploration.explore_string import ExploreString
from xpresso.ai.core.data.exploration.explore_text import ExploreText
from xpresso.ai.core.data.exploration.render_exploration import \
    RenderExploration
from xpresso.ai.core.logging.xpr_log import XprLogger

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class StructuredDatasetExplorer:

    def __init__(self, dataset):
        self.dataset = dataset
        if get_dataframe_size(self.dataset.data) > MAX_DATASET_SIZE_GB:
            error_msg = "Size more than {}GB not supported for exploration. " \
                        "Try sampling the data.".format(MAX_DATASET_SIZE_GB)
            logger.error(error_msg)
            raise ExceedsDataSize(error_msg)

    def understand(self, verbose=True):
        """
        Understands and assigns the datatype to the attributes for
        structured dataset
        Args:
            verbose('bool'): If True then renders the output
        """
        if self.dataset.type != DatasetType.STRUCTURED:
            logger.error("Unacceptable Data type provided. Type {} is "
                         "not supported".format(self.dataset.type))
            raise InvalidDatatypeException("Provided Data Type : {} not "
                                           "supported".format(
                self.dataset.type))
        self.understand_attributes(self.dataset.data)
        if self.dataset.type == DatasetType.STRUCTURED:
            for attr in self.dataset.info.attributeInfo:
                if attr.type == DataType.NUMERIC.value:
                    self.dataset.data[attr.name] = pd.to_numeric(
                        self.dataset.data[attr.name], errors="coerce")

        RenderExploration(self.dataset).render_understand(verbose=verbose)

    def explore_univariate(self, verbose=True, to_excel=False,
                           validity_threshold=None,
                           output_path=EXPLORER_OUTPUT_PATH,
                           file_name=EXPLORE_UNIVARIATE_FILENAME,
                           bins=DEFAULT_PROBABILITY_BINS):
        """
        Univariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
            validity_threshold(int): percent value for garbage threshold
            bins(int): No. of buckets for bar graph, default 20
        """

        if self.dataset.type != DatasetType.STRUCTURED or not isinstance(
                self.dataset.type, DatasetType):
            logger.warning(
                "Unsupported dataset type provided to explore_univariate")
            raise InvalidDatatypeException("Provided Data Type : {} not "
                                           "supported".format(
                self.dataset.type))

        self.populate_attribute(validity_threshold,
                                bins=bins)

        RenderExploration(self.dataset).render_univariate(
            verbose=verbose, to_excel=to_excel, output_path=output_path,
            file_name=file_name)

    def explore_multivariate(self, verbose=True, to_excel=False,
                             output_path=EXPLORER_OUTPUT_PATH,
                             file_name=EXPLORE_MULTIVARIATE_FILENAME):
        """
        Multivariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
        """

        if self.dataset.type != DatasetType.STRUCTURED or not isinstance(
                self.dataset.type, DatasetType):
            logger.warning(
                "Unsupported datatype provided to explore_multivariate")
            raise InvalidDatatypeException("Provided Data Type : {} not "
                                           "supported".format(
                self.dataset.type))

        self.populate_metric(self.dataset.data, self.dataset.type)

        RenderExploration(self.dataset).render_multivariate(
            verbose=verbose, to_excel=to_excel, output_path=output_path,
            file_name=file_name)

    def explore_unstructured(self, verbose=True, to_excel=False):
        """Unsupported"""
        logger.warning(
            "Unsupported datatype provided to explore_unstructured")
        raise InvalidDatatypeException("Provided Data Type : {} not "
                                       "supported".format(
            self.dataset.type))

    def populate_attribute(self, threshold,
                           bins=DEFAULT_PROBABILITY_BINS):
        # For structured datatype
        print("\nStarting UniVariate Exploration:\n")
        for attr in tqdm.tqdm(self.dataset.info.attributeInfo):
            self.populate(attr, self.dataset.data[attr.name], threshold,
                          bins=bins)

    def populate(self, attr, data, threshold, bins=DEFAULT_PROBABILITY_BINS):
        if str(attr.type) == "numeric":
            attr.metrics = ExploreNumeric(data, threshold,
                                          probability_dist_bins=bins).populate_numeric()
        elif str(attr.type) == "ordinal" or attr.type == "nominal":
            attr.metrics = ExploreCategory(data).populate_category()
        elif str(attr.type) == "date":
            attr.metrics = ExploreDate(data).populate_date()
        elif str(attr.type) == "string":
            attr.metrics = ExploreString(data).populate_string()
        elif str(attr.type) == "text":
            attr.metrics = ExploreText(data).populate_text()
        (na_count, na_count_percentage, missing_count,
         missing_count_percentage) = self.na_analysis_univariate(data)
        attr.metrics["na_count"] = na_count
        attr.metrics["na_count_percentage"] = na_count_percentage
        attr.metrics["missing_count"] = missing_count
        attr.metrics["missing_count_percentage"] = missing_count_percentage

    @staticmethod
    def na_analysis_univariate(data):
        num_rows = float(data.size)
        na_count = float(data.isna().sum())

        na_count_percentage = round((na_count / num_rows) * 100,
                                    DECIMAL_PRECISION)
        missing_count = (data == "").sum()
        if not missing_count:
            missing_count = 0
        else:
            missing_count = float(missing_count)

        missing_count_percentage = round((missing_count / num_rows) * 100,
                                         DECIMAL_PRECISION)

        return (na_count,
                na_count_percentage,
                missing_count,
                missing_count_percentage)

    @staticmethod
    def na_analysis_multivariate(data):
        """Calculates the NA fields and missing fields in the data
        Args:
            data (:obj:`Dataframe`): the data in which number of
                missing fields and null fields are to be calculated
        Return:
            na_count (:int): Number of NA fields
            na_count_percentage (:float): Percentage of NA fields
            missing_count (:int): Number of missing data
            missing_count_percentage (:float): Percentage of missing data
        """
        num_rows = float(data.size)
        print(data.isnull().sum().sum())
        na_count = sum(data.isnull().apply(lambda x: all(x), axis=1))
        na_count_percentage = round(float(na_count / num_rows) * 100,
                                    DECIMAL_PRECISION)
        missing_count = int((data == "").sum().sum())
        missing_count_percentage = round(float(missing_count / num_rows) * 100,
                                         DECIMAL_PRECISION)

        return (na_count, na_count_percentage, missing_count,
                missing_count_percentage)

    def populate_metric(self, data, data_type):
        # For structured datatype
        print("Starting Multivariate Exploration:\n")
        data_copy = data
        self.dataset.info.metrics["num_records"] = len(data_copy)
        self.dataset.info.metrics["num_attributes"] = len(data_copy.columns)

        (na_count, na_count_percentage, missing_count,
         missing_count_percentage) = self.na_analysis_multivariate(
            data_copy)

        self.dataset.info.metrics["na_count"] = na_count
        self.dataset.info.metrics["na_count_percentage"] = na_count_percentage
        self.dataset.info.metrics["missing_count"] = missing_count
        self.dataset.info.metrics[
            "missing_count_percentage"] = missing_count_percentage

        (duplicates_count, duplicates_count_percentage,
         duplicate_rows_count) = self.duplicates_analysis(data_copy)

        self.dataset.info.metrics["duplicate_count"] = duplicates_count
        self.dataset.info.metrics[
            "duplicate_count_percentage"] = duplicates_count_percentage
        self.dataset.info.metrics["duplicate_rows_count"] = duplicate_rows_count

        numeric_field = list()
        nominal_field = list()
        ordinal_field = list()
        date_field = list()
        string_field = list()
        text_field = list()
        for val in self.dataset.info.attributeInfo:
            if val.type == DataType.NUMERIC.value:
                numeric_field.append(val.name)
            elif val.type == DataType.NOMINAL.value:
                nominal_field.append(val.name)
            elif val.type == DataType.ORDINAL.value:
                ordinal_field.append(val.name)
            elif val.type == DataType.DATE.value:
                date_field.append(val.name)
            elif val.type == DataType.STRING.value:
                string_field.append(val.name)
            elif val.type == DataType.TEXT.value:
                text_field.append(val.name)
        self.dataset.info.metrics["numeric_attributes"] = numeric_field
        self.dataset.info.metrics["num_numeric"] = len(numeric_field)
        self.dataset.info.metrics["num_nominal"] = len(nominal_field)
        self.dataset.info.metrics["num_ordinal"] = len(ordinal_field)
        self.dataset.info.metrics["num_date"] = len(date_field)
        self.dataset.info.metrics["num_string"] = len(string_field)
        self.dataset.info.metrics["num_text"] = len(text_field)

        if len(numeric_field) != 0:
            self.dataset.info.metrics["pearson"] = data[numeric_field].corr(
                method="pearson").round(DECIMAL_PRECISION).unstack().to_dict()

        if len(numeric_field + ordinal_field) != 0:
            self.dataset.info.metrics["spearman"] = data[numeric_field +
                                                         ordinal_field].corr(
                method="spearman").round(DECIMAL_PRECISION).unstack().to_dict()

        nominal_nominal_comb = list(
            itertools.product(nominal_field, nominal_field))
        nominal_ordinal_comb = list(itertools.product(nominal_field,
                                                      ordinal_field))
        chisquare_combination = nominal_nominal_comb + nominal_ordinal_comb

        if len(chisquare_combination) > 0:
            self.dataset.info.metrics["chi_square"] = dict()

        for val in tqdm.tqdm(chisquare_combination):
            self.dataset.info.metrics["chi_square"][val] = self.ChiSquareTest(
                data, val[0], val[1])
            try:
                self.dataset.info.metrics["chi_square"][
                    val] = self.ChiSquareTest(data, val[0], val[1])
            except IndexError:
                pass

    @staticmethod
    def duplicates_analysis(df):
        """Identifying count of duplicate rows in the data
        Args:
            df(:obj:`DataFrame`): data in which count of duplicate rows is to be
                                    added
        Returns:
            list_tuples(list): list of tuples containing a row and it's
                                corresponding duplicate count
        """
        # a new column called "duplicate_rows_count" is created
        temp_data = pd.DataFrame(
            df.groupby(df.columns.to_list(), as_index=False).size())
        data = temp_data.rename(
            columns={0: "duplicate_rows_count"}).reset_index()

        # a list of duplicate rows count
        list_dup_row_count = data.to_dict(orient="list")["duplicate_rows_count"]

        # dropping the newly made column
        non_dup_data = data.drop(columns="duplicate_rows_count")

        # creating a list of tuples containing a row and it's corresponding
        # duplicate count
        list_tuples = []
        for i in range(non_dup_data.shape[0]):
            tup = (list(non_dup_data.iloc[i, :]), list_dup_row_count[i] - 1)
            list_tuples.append(tup)

        num_rows = float(df.size)
        duplicates_count = sum(df.duplicated())
        duplicates_count_percentage = round(float(duplicates_count / num_rows) *
                                            100, DECIMAL_PRECISION)

        return duplicates_count, duplicates_count_percentage, list_tuples

    @staticmethod
    def ChiSquareTest(df, x, y):
        """Calculates the chi square correlation between two attributes
        Args:
             x(:list): List of values of one attribute
             y(:list): List of values of another attribute
        Returns:
            (int): Chi-square correlation coefficient
         """
        x = df[x].astype(str)
        y = df[y].astype(str)

        dfObserved = pd.crosstab(y, x)
        chi2, p_value, dof, expected = stats.chi2_contingency(dfObserved.values)
        return round(p_value, DECIMAL_PRECISION)

    def understand_attributes(self, data):
        self.dataset.info.attributeInfo = list(map(lambda x: AttributeInfo(x),
                                                   data.columns))
        print("\nStarting Data Understanding:\n")
        for attr in tqdm.tqdm(self.dataset.info.attributeInfo):
            attr.dtype = data[attr.name].dtype
            attr.type, attr.dtype = self.find_attr_type(data[attr.name],
                                                        attr.dtype)
            if attr.type is DataType.DATE.value:
                data[attr.name] = data[attr.name].apply(
                    lambda x: x if pd.isna(x) else self.parse_date(x))
                attr.dtype = data[attr.name].dtype

    @staticmethod
    def is_date(date_values):
        is_date_bool = list()
        special_characters_in_date = re.compile("[/:,-. ]")
        for date in date_values.iteritems():
            try:
                data = pd.to_datetime(date[1], errors="coerce")
                if data is pd.NaT:
                    raise InvalidDateFormat
                if special_characters_in_date.search(date[1]) is None:
                    raise InvalidDateFormat
                if len(date[1]) > MAX_DATE_LENGTH or len(
                        date[1]) < MIN_DATE_LENGTH:
                    raise InvalidDateFormat
                is_date_bool.append(True)
            except ValueError:
                is_date_bool.append(False)
            except InvalidDateFormat as e:
                is_date_bool.append(False)

        percent_threshold = 0.95
        ret = True if sum(is_date_bool) > len(
            is_date_bool) * percent_threshold else False
        return ret

    @staticmethod
    def parse_date(date_value):
        """
        It parses date_value to date format if possible, else returns the same
        value
        Args:
            date_value(:str): String of data
        Returns:
            Parsed value of date_value to date if possible
        """
        try:
            ret = pd.to_datetime(date_value, errors="ignore")
        except ValueError as e:
            return date_value
        except Exception as e:
            return date_value
        return ret

    # Below method finds the data type of the attributes
    @staticmethod
    def find_attr_type(data, dtype, threshold=5, length_threshold=50):
        num_rows = float(data.size)
        unique_values = data.unique().tolist()
        unique_proportion = (float(len(unique_values)) / num_rows) * 100
        dtype = str(dtype)

        data_type = DataType.STRING.value
        if DataType.FLOAT.value in dtype or DataType.INT.value in dtype:
            if unique_proportion < threshold:
                data_type = DataType.NOMINAL.value

            else:
                data_type = DataType.NUMERIC.value

        elif DataType.OBJECT.value in dtype:
            max_length = data[~data.isna()].map(
                lambda x: len(x) if type(x) == str else 0).max()
            temp_data = pd.to_numeric(data, errors="coerce")
            na_count = temp_data.isna().sum()
            na_percent = na_count / num_rows * 100

            if na_percent < DEFAULT_GARBAGE_THRESHOLD:
                dtype = temp_data.dtype

                if unique_proportion < threshold:
                    data_type = DataType.NOMINAL.value
                else:
                    data_type = DataType.NUMERIC.value

            elif StructuredDatasetExplorer.is_date(data[~data.isna()]):
                data_type = DataType.DATE.value

            elif unique_proportion < threshold:
                data_type = DataType.NOMINAL.value

            elif max_length < length_threshold:
                data_type = DataType.STRING.value

            else:
                data_type = DataType.TEXT.value

        elif DataType.BOOL.value in dtype:
            data_type = DataType.NOMINAL.value

        return data_type, dtype
